CC = gcc
COMPILE_FLAGS = -Wall
DEBUG = #empty variable

all: binary

binary: bin/hyMaIntp.o
	${CC} ${COMPILE_FLAGS}  $< -o bin/hyMaIntp

bin/hyMaIntp.o: src/hyMaIntp.c
ifndef DEBUG
	${CC} ${COMPILE_FLAGS} -c $< -o $@
else
	${CC} ${COMPILE_FLAGS} -ggdb -c $< -o $@
endif

.PHONY: clean
clean:
	rm ./hyMaIntp.o ./hyMaIntp 2> /dev/null
