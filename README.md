# hyMaIntp

Interpreter for the hypothetical machine used in the course "Grundlagen der Informatik I (I-110)" at the HTW Dresden.

## specification

The hypothetical machine uses operations of four byte. An accumulator (AC) is used to store a current value. Values can be loaded from variables which are defined after the code section. The provided instructions are:

**OP  instruction   description**  
58	LOAD a        loads the value in variable a in the AC  
50	STORE a       stores the value of the AC in the variable a  
5A	ADD a         adds the variable a to the value in AC  
5B	SUB a         subtracts the variable a from the value in AC  
5C	MUL a         multiplies the variable a with the value in AC  
5D	QUOT a        divides the value in AC with the value in variable a  
5E	MODULO a      calculates the remainder of the operation AC / a  
47	JUMP L1     	jumps to the label L1 as next instruction  
48	JUMPZERO L1 	jumps to the label L1 as next instruction if the value in AC is equal to 0  
45	STOP  	      marks the end of the program  

Variables are defined in the following way:

	5 {five}

Variable definitions are only allowed after the STOP operation.

A sample programm for subtracting 1 from 5 in a loop until the result is 0 looks like:	
	
	L2  LOAD five
	    JUMPZERO L1
	    SUB one
	    STORE five
	    JUMP L2
	L1  STOP
	   5 {five}
	   1 {one}

## usage

The Interpreter can be run in the following way.

	hyMaIntp [--norun] <filename.txt>

## license

This project is licensed under the [GNU Public License v3](https://www.gnu.org/licenses/gpl-3.0.en.html).

## another project working with the hypothetic machine

There is also a compiler for this machine with a more detailed specification of the machine itself. The compiler project can be found [here (https://gitlab.com/eman.guenther/hymac)](https://gitlab.com/eman.guenther/hymac).

