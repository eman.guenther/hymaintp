/*
hyMaIntp v1.0 (interpreter for the language of the "hypothetische Maschine", used at the HTW Dresden in modul I-110)
Copyright (C) 2018 Emanuel Günther <eman.guenther@gmx.de>

This program is free software: you can redistribute it and/or modify
it under the terms of the GNU General Public License as published by
the Free Software Foundation, either version 3 of the License, or
(at your option) any later version.

This program is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
GNU General Public License for more details.

You should have received a copy of the GNU General Public License
along with this program.  If not, see <https://www.gnu.org/licenses/>.
*/

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>

#define DEBUG 0 //set to 1 for debugging information
/*  exit codes:
    -1 - critical error
	 0 - everything was right
	 1 - wrong parameters
     2 - file error
     3 - lexer error
     4 - parser error
     5 - interpreter error
*/

//some special data types and typedefs
enum type { label, operator, variable, number, linebreak, vardef, endoffile, error }; //vardef: variable definition (in the code: {n})
struct token {
    enum type tokentype;
    char data[9];
};
typedef enum type type;
typedef struct token token;
struct tkelem {
    struct tkelem *prev;
    type tokentype;
    char data[9];
    struct tkelem *next;
};
typedef struct tkelem tkelem;
struct varelem {
    char varname[5];
    int varvalue;
    struct varelem *next;
};
typedef struct varelem varelem;
struct lbelem {
    char lbname[3];
    tkelem *link;
    struct lbelem *next;
};
typedef struct lbelem lbelem;

//general functions
void print_usage(char*);
int check_parameters(int, char **, int*);
char* readmore(FILE*, char*, int);
token lex(FILE*);
tkelem* parse(FILE*); //check for correct order of tokens
void interpreter(FILE*, tkelem*, int); //check for vardefs (used variables should be defined ONCE, same with labels); interpretation (if run == 1)
int dbg_prompt(int, varelem*, tkelem*, tkelem*);
void show_code(tkelem*, tkelem*);
void exiting(tkelem*, FILE *, varelem*, lbelem*, int); //function to close files etc

//functions for working with doubly linked list (tokens)
tkelem* insert_tkelem(tkelem*, token*);
void deleteall_tkelem(tkelem*);

//functions for working with singly linked list (int variables, labels)
varelem* insert_varelem(varelem*, char*, int);
void deleteall_varelem(varelem*);
lbelem* insert_lbelem(lbelem*, char*, tkelem*);
void deleteall_lbelem(lbelem*);

//additional function and additional switch: codegen() and -nocode (no code generation)?
int main(int argc, char *argv[]){
    int run; //variable for disabling the execution of the code
    int arg_file_index = 1; //index for filename in argv
    
    //check_parameters changes arg_file_index!
    run = check_parameters(argc, argv, &arg_file_index); //check for wrong parameters
    tkelem *parselist = NULL;
    
    FILE *fpsrc; //source file
    fpsrc = fopen(argv[arg_file_index], "rt");
    if(fpsrc == NULL){
        printf("File could not be opened.\n");
        exiting(NULL, fpsrc, NULL, NULL, 2);
    }
    
    printf("hyMaIntp v1.0 Copyright (C) 2018 Emanuel Günther\n");
    printf("This program comes with ABSOLUTELY NO WARRANTY.\nThis is free software, and you are welcome to redistribute it under certain conditions; type 'shcw' for details.\n\n");
    
    printf("Loaded file %s.\n", argv[arg_file_index]);
    
    //insert test cases here
    
    parselist = parse(fpsrc);
    interpreter(fpsrc, parselist, run);
    
    //clean-up
    exiting(parselist, fpsrc, NULL, NULL, 0);
    return 0;
}

void print_usage(char *progname){
    printf("Usage: %s [--norun] <filename.txt>\n", progname);
}

int check_parameters(int argc, char *argv[], int *arg_file_index){
    int run=1, file=0, len;
    char ending[5];
    
    if(argc < 2){
        printf("Too little arguments.\n");
        print_usage(argv[0]);
        exiting(NULL, NULL, NULL, NULL, 1);
    }
    if(argc > 3){
        printf("Too many arguments.\n");
        print_usage(argv[0]);
        exiting(NULL, NULL, NULL, NULL, 1);
    }
    
    //strcmp returns 0 if true
	if(!strcmp(argv[1], "--norun")){ //if equal
        run = 0;
		*arg_file_index = 2;
        if(argc == 2){
            printf("File is missing as parameter.\n");
            print_usage(argv[0]);
            exiting(NULL, NULL, NULL, NULL, 1);
        }
    } else { //if not equal
		if(argc == 3){
            if(!strcmp(argv[2], "--norun")){
                run = 0;
            } else { //if none of both parameters is the switch "--norun"
                printf("Wrong parameters.\n");
                print_usage(argv[0]);
                exiting(NULL, NULL, NULL, NULL, 1);
            }
        }
	}
	
	len = strlen(argv[*arg_file_index]);
	for(int i = len-4; i < len; i++){
		ending[i-len+4] = argv[*arg_file_index][i];
	}
	ending[4] = '\0';
	if(strcmp(ending,".txt")){ //if file does not end with .txt
		printf("Wrong filetype.\n");
		print_usage(argv[0]);
		exiting(NULL, NULL, NULL, NULL, 1);
	} else {
		//test should work for linux and windows?!
        //F_OK would be the alternative
        //R_OK: file can be read; F_OK: file exists
        if(DEBUG) printf("check_parameters(): access: Not sure, if this is possible under Windows.\n");
        if(access( argv[*arg_file_index], R_OK ) != -1 ) { //tests for existence of file
            file++;
        } else {
            //file doesn't exist or cannot be read
            printf("File doesn't exist. Please state a valid filename.\n");
            print_usage(argv[0]);
            exiting(NULL, NULL, NULL, NULL, 1);
        }
	}
    
	if(file == 1){
        return run;
	} else {
		printf("Something went terribly wrong!\n");
		exiting(NULL, NULL, NULL, NULL, -1);
	}
    return run;
}

//writes to *buf
char* readmore(FILE *fpsrc, char *buf, int cnt){
    if((cnt < 0)||(cnt > 8)) return NULL;
    char c;
    //c = fgetc(fpsrc); //dd
    
    while(cnt < 8){
        c = fgetc(fpsrc);
        if((c == ' ')||(c == '\n')||(c == EOF)||(c == 015)){
            if(c != EOF) fseek(fpsrc, -1, SEEK_CUR);
            break;
        }
        buf[cnt] = c;
        cnt++;
        //c = fgetc(fpsrc); //dd
    }
    
    buf[cnt] = '\0'; //cnt is after chars, but maximum 8
    //if(cnt == 8) fseek(fpsrc, -1, SEEK_CUR); //dd
    return buf;
}

token lex(FILE *fpsrc){
    char c;
    token newtoken;
    char buf[9]; //buffer for reading
    buf[0] = '\0';
    
    c = fgetc(fpsrc);
    
    if((c == ' ')||(c == 015)){ //015 is part of WIN linebreaks: 015 012, where 012 == '\n'
        return lex(fpsrc); //take one more step
    } else if(c == '\n'){
        newtoken.tokentype = linebreak;
    } else if(c == '{'){
        newtoken.tokentype = vardef;
        buf[0] = c;
        readmore(fpsrc, buf, 1);
        for(int i = 0; i < 8; i++){ //check for '}'
            if(buf[i+1] == '\0'){
                if(buf[i] != '}') {
                    newtoken.tokentype = error;
                    printf("Missing '}' at vardef token.\n");
                    buf[i] = buf[i+1];
                } else {
                    buf[i-1] = '\0'; //[i-1] is index from first '}'
                }
                break;
            }
            buf[i] = buf[i+1];
        }
    } else if((c >= '0')&&(c <='9')){
        newtoken.tokentype = number;
        buf[0] = c;
        readmore(fpsrc, buf, 1);
    } else if(c == 'L'){
        buf[0] = c;
        c = fgetc(fpsrc);
        if((c >='0')&&(c <= '9')){ //ascii number
            newtoken.tokentype = label;
            buf[1] = c;
            buf[2] = '\0';
        } else if((c >= 'A')&&(c <= 'Z')){ //ascii big letter
            newtoken.tokentype = operator;
            buf[1] = c;
            readmore(fpsrc, buf, 2);
        } else {
            fseek(fpsrc, -1, SEEK_CUR);
            newtoken.tokentype = error;
            buf[1] = c;
            buf[2] = '\0';
        }
    } else if((c >= 'A')&&(c <= 'Z')){ //ascii big letter
        newtoken.tokentype = operator;
        buf[0] = c;
        readmore(fpsrc, buf, 1);
    } else if((c >= 'a')&&(c <= 'z')){ //ascii lower letter
        newtoken.tokentype = variable;
        buf[0] = c;
        readmore(fpsrc, buf, 1);
    } else if(c == EOF){
        newtoken.tokentype = endoffile;
    } else if(c == '/'){
        c = fgetc(fpsrc);
        if(c == '/'){
            while(c != '\n'){
                c = fgetc(fpsrc);
            }
            fseek(fpsrc, -1, SEEK_CUR); //-1, so that the next char is \n and can be read
            return lex(fpsrc);
        } else {
            newtoken.tokentype = error;
            buf[0] = '/';
            buf[1] = '\0';
        }
    } else if(c == '\t'){
        newtoken.tokentype = error;
        printf("Tabulators are not allowed. Please use single whitespaces.\n");
        buf[0] = c;
        buf[1] = '\0';
    } else {
        newtoken.tokentype = error;
        buf[0] = c;
        buf[1] = '\0';
    }
    strcpy(newtoken.data, buf);
    
    return newtoken;
}

tkelem* parse(FILE *fpsrc){
    tkelem *tokenlist = NULL; //doubly linked list of tokens, anchor
    token nulltoken = { linebreak, "\0" };
    tokenlist = insert_tkelem(tokenlist, &nulltoken); //first token is nulltoken (linebreak)
    if(tokenlist == NULL) exiting(tokenlist, fpsrc, NULL, NULL, 4);
    tkelem *tkptr = tokenlist; //temporary pointer to last token in list
    token tktmp; //current token from lexer
    int line = 1; //number of the line in the code
    int gblstop = 0; //global stop true/false [1/0]
    
    do{
        tktmp = lex(fpsrc);
        switch(gblstop){
            case 0: //before STOP: label, operator, variable
                switch(tktmp.tokentype){
                    case label: //just after linebreak, JUMP or JUMPZERO
                        if(! ( (tkptr->tokentype == linebreak) || ( (tkptr->tokentype == operator) && ( (!strcmp(tkptr->data, "JUMP")) || (!strcmp(tkptr->data, "JUMPZERO")) ) ) ) ){
                            printf("Wrong syntax: label after %d token in line %d.\n", tkptr->tokentype, line);
                            exiting(tokenlist, fpsrc, NULL, NULL, 4);
                        }
                        break;
                    case operator: //just after linebreak or label
                        if((tkptr->tokentype != linebreak)&&(tkptr->tokentype != label)){
                            printf("Wrong syntax: operator after %d token in line %d.\n", tkptr->tokentype, line);
                            exiting(tokenlist, fpsrc, NULL, NULL, 4);
                        }
                        if(strcmp(tktmp.data, "LOAD")&&strcmp(tktmp.data, "STORE")&&strcmp(tktmp.data, "ADD")&&strcmp(tktmp.data, "SUB")&&strcmp(tktmp.data, "DIV")&&strcmp(tktmp.data, "MOD")&&strcmp(tktmp.data, "MUL")&&strcmp(tktmp.data, "JUMP")&&strcmp(tktmp.data, "JUMPZERO")&&strcmp(tktmp.data, "STOP")){
                            printf("Unrecognized operator token %s in line %d.\n", tktmp.data, line);
                            exiting(tokenlist, fpsrc, NULL, NULL, 4);
                        }
                        if(!strcmp(tktmp.data, "STOP")) gblstop = 1;
                        break;
                    case variable: //just after operator token
                        if(tkptr->tokentype != operator){
                            printf("Wrong syntax: variable after %d token in line %d.\n", tkptr->tokentype, line);
                            exiting(tokenlist, fpsrc, NULL, NULL, 4);
                        }
                        break;
                    case number:
                    case vardef:
                    case endoffile:
                        printf("Wrong syntax: %d token in code part in line %d.\n", tktmp.tokentype, line);
                        exiting(tokenlist, fpsrc, NULL, NULL, 4);
                        break;
                    default: break; //linebreak and error
                }
                break;
            case 1: //after STOP: number, vardef, endoffile
                switch(tktmp.tokentype){
                    case number: //just after global stop and local linebreak
                        if(tkptr->tokentype != linebreak){
                            printf("Wrong syntax: number after %d token in line %d.\n", tkptr->tokentype, line);
                            exiting(tokenlist, fpsrc, NULL, NULL, 4);
                        }
                        //check for just numbers
                        for(int i = strlen(tktmp.data)-1; i >= 0; i--){
                            if((tktmp.data[i] < '0')||(tktmp.data[i] > '9')){
                                tktmp.tokentype = error;
                                break;
                            }
                        }
                        break;
                    case vardef: //just after global stop and local number
                        if(tkptr->tokentype != number){
                            printf("Wrong syntax: vardef after %d token in line %d.\n", tkptr->tokentype, line);
                            exiting(tokenlist, fpsrc, NULL, NULL, 4);
                        }
                        break;
                    case endoffile: //just after global stop so that program has an end
                        if((tkptr->tokentype != vardef)&&strcmp(tkptr->data, "STOP")){ //if tokens are missing (e.g. variable, label, ...)
                        printf("Wrong syntax: EOF after %d token in line %d.\n", tktmp.tokentype, line);
                        exiting(tokenlist, fpsrc, NULL, NULL, 4);
                        }
                        break;
                    case label:
                    case operator:
                    case variable:
                        printf("Wrong syntax: %d token in data part in line %d.\n", tktmp.tokentype, line);
                        exiting(tokenlist, fpsrc, NULL, NULL, 4);
                        break;
                    default: break; //linebreak and error
                }
                break;
        }
        
        //independent from STOP operator
        if(tktmp.tokentype == linebreak){ //just after label, variable, stop or vardef
            if((tkptr->tokentype != label)&&(tkptr->tokentype != variable)&&(tkptr->tokentype != vardef)&&strcmp(tkptr->data, "STOP")){ //if tokens are missing (e.g. variable, label, ...)
                printf("Wrong syntax: Linebreak after %d token in line %d.\n", tktmp.tokentype, line);
                exiting(tokenlist, fpsrc, NULL, NULL, 4);
            }
            line++;
        } else if (tktmp.tokentype == error){
            printf("Unrecognised token: %s\n", tktmp.data);
            exiting(tokenlist, fpsrc, NULL, NULL, 4);
        }
        
        //if everything ok
        tkptr = insert_tkelem(tkptr, &tktmp);
        if(tkptr == NULL) exiting(tokenlist, fpsrc, NULL, NULL, 4);
    } while (tktmp.tokentype != endoffile);
    
    return tokenlist;
}

void interpreter(FILE *fpsrc, tkelem *parselist, int run){
    int ac; //accumulator
    int steps = 1; //run in steps (next)? [0/1]
    int dbgret = 1; //return value of dbg_prompt
    varelem *varlist = NULL; //list of variables
    varelem *vartmp = NULL;
    lbelem *lblist = NULL; //list of labels
    lbelem *lbtmp = NULL;
    tkelem *rc = parselist; //run commands
    tkelem *jmp = NULL;
    
    //check for label and variable definition
    tkelem *pt = parselist;
    while(pt != NULL){ //the whole list
        if(pt->tokentype == label){
            if(pt->prev->tokentype == linebreak){
                lbtmp = lblist;
                while(lbtmp != NULL){
                    if(!strcmp(lbtmp->lbname, pt->data)){ //if already in list
                        lbtmp->link = pt; //link shows on current element, because it's the destination
                        break;
                    }
                    lbtmp = lbtmp->next;
                }
                if(lbtmp == NULL){ //not in list
                    lblist = insert_lbelem(lblist, pt->data, pt);
                    if(lblist == NULL) exiting(parselist, fpsrc, varlist, lblist, 5);
                }                
            } else if(pt->prev->tokentype == operator){
                lbtmp = lblist;
                while(lbtmp != NULL){
                    if(!strcmp(lbtmp->lbname, pt->data)) break; //if already in list
                    lbtmp = lbtmp->next;
                }
                if(lbtmp == NULL){ //not in list
                    lblist = insert_lbelem(lblist, pt->data, NULL);
                    if(lblist == NULL) exiting(parselist, fpsrc, varlist, lblist, 5);
                }
            }
        } else if(pt->tokentype == variable){
            vartmp = varlist;
            while(vartmp != NULL){
                if(!strcmp(vartmp->varname, pt->data)) break; //if already in list
                vartmp = vartmp->next;
            }
            if(vartmp == NULL){ //not in list
                varlist = insert_varelem(varlist, pt->data, -32768);
                if(varlist == NULL) exiting(parselist, fpsrc, varlist, lblist, 5);
            }
        } else if(pt->tokentype == vardef){
            vartmp = varlist;
            while(vartmp != NULL){
                if(!strcmp(vartmp->varname, pt->data)){//if already in list
                    vartmp->varvalue = atoi(pt->prev->data); //number token in front of vardef token
                    break;
                }
                vartmp = vartmp->next;
            }
            if(vartmp == NULL){ //not in list
                printf("Warning: unused variable %s.\n", pt->data);
                varlist = insert_varelem(varlist, pt->data, atoi(pt->prev->data));
                if(varlist == NULL) exiting(parselist, fpsrc, varlist, lblist, 5);
            }
        }
        pt = pt->next;
    }
    
    vartmp = varlist;
    while(vartmp != NULL){
        if(vartmp->varvalue == -32768){
            printf("Error: Variable %s was not defined.\n", vartmp->varname);
            exiting(parselist, fpsrc, varlist, lblist, 5);
        }
        vartmp = vartmp->next;
    }
    
    lbtmp = lblist;
    while(lbtmp != NULL){
        if(lbtmp->link == NULL){
            printf("Error: Label %s was not defined.\n", lbtmp->lbname);
            exiting(parselist, fpsrc, varlist, lblist, 5);
        }
        lbtmp = lbtmp->next;
    }
    
    if(run == 0){
        exiting(parselist, fpsrc, varlist, lblist, 0);
    }
    
    printf("Starting with interpretation.\n");
    
    //reset tmp pts
    vartmp = varlist;
    lbtmp = lblist;
    
    //interpretation
    while(rc->tokentype != endoffile){
        if(rc->tokentype == operator){
            if(!strcmp(rc->data, "LOAD")){
                while(strcmp(vartmp->varname,rc->next->data)) vartmp = vartmp->next;
                ac = vartmp->varvalue;
            } else if(!strcmp(rc->data, "STORE")){
                while(strcmp(vartmp->varname,rc->next->data)) vartmp = vartmp->next;
                vartmp->varvalue = ac;
            } else if(!strcmp(rc->data, "ADD")){
                while(strcmp(vartmp->varname,rc->next->data)) vartmp = vartmp->next;
                ac += vartmp->varvalue;
            } else if(!strcmp(rc->data, "SUB")){
                while(strcmp(vartmp->varname,rc->next->data)) vartmp = vartmp->next;
                ac -= vartmp->varvalue;
            } else if(!strcmp(rc->data, "MUL")){
                while(strcmp(vartmp->varname,rc->next->data)) vartmp = vartmp->next;
                ac *= vartmp->varvalue;
            } else if(!strcmp(rc->data, "DIV")){
                while(strcmp(vartmp->varname,rc->next->data)) vartmp = vartmp->next;
                ac /= vartmp->varvalue;
            } else if(!strcmp(rc->data, "MOD")){
                while(strcmp(vartmp->varname,rc->next->data)) vartmp = vartmp->next;
                ac %= vartmp->varvalue;
            } else if(!strcmp(rc->data, "JUMP")){
                while(strcmp(lbtmp->lbname,rc->next->data)) lbtmp = lbtmp->next;
                jmp = lbtmp->link;
            } else if(!strcmp(rc->data, "JUMPZERO")){
                if(ac == 0){
                    while(strcmp(lbtmp->lbname,rc->next->data)) lbtmp = lbtmp->next;
                    jmp = lbtmp->link;
                } else {
                    jmp = rc->next;
                }
            } else if(!strcmp(rc->data, "STOP")){
                jmp = rc;
                while(jmp->tokentype != endoffile) jmp = jmp->next; //goto endoffile
            }
            
            vartmp = varlist; //reset
            lbtmp = lblist; //reset
            
            //debugger
            if((steps)||(!strcmp(rc->data, "STOP"))){
                dbgret = dbg_prompt(ac, vartmp, parselist, rc);
                if(dbgret == 0){
                    steps = 0;
                } else if(dbgret == 2){
                    exiting(parselist, fpsrc, varlist, lblist, 0);
                } //if dbgret == 1: nothing happens
            }
            
            //next elem
            if(strcmp(rc->data, "JUMPZERO")&&strcmp(rc->data, "JUMP")){ //is false if one of them matches
                rc = rc->next;
            } else {
                rc = jmp;
            }
        } else { //if rc->tokentype is not operator
            rc = rc->next;
        }
    }
}

int dbg_prompt(int ac, varelem *varlist, tkelem *parselist, tkelem *position){
    char dbg[5]; //input buffer for simple debugging
    varelem *vartmp = varlist;
    
    printf(">>> ");
    
    #ifdef _WIN32
    fflush(stdin);
    #endif
    if(scanf("%4s", (char*)&dbg) < 1){
        printf("Unrecognized command. Type 'help' for more information.\n");
        return dbg_prompt(ac, vartmp, parselist, position);
    }
    dbg[4] = '\0';
    
    if((!strcmp(dbg, "run"))||(!strcmp(dbg, "r"))){
        return 0; //run fluently without interrupts
    } else if((!strcmp(dbg, "next"))||(!strcmp(dbg, "n"))){
        return 1;
    } else if((!strcmp(dbg, "info"))||(!strcmp(dbg, "i"))){
        printf("ac: %d\n", ac);
        while(vartmp != NULL){
            printf("variable %s: %d\n", vartmp->varname, vartmp->varvalue);
            vartmp = vartmp->next;
        }
        vartmp = varlist;
    } else if((!strcmp(dbg, "help"))||(!strcmp(dbg, "h"))){
        printf("help:\n run/r - run until program ends\nnext/n - make one step (next line)\ninfo/i - show status of variables\ncode/c - show full code and current position\nhelp/h - show this help text\nexit   - quit the program\n");
    } else if(!strcmp(dbg, "exit")){
        return 2; //initiates exiting
    } else if((!strcmp(dbg, "code"))||(!strcmp(dbg, "c"))){
        show_code(parselist, position);
    } else if(!strcmp(dbg, "shcw")){
	printf("hyMaIntp v1.0 Copyright (C) 2018 Emanuel Günther\n\n");
        printf("\nThis program is free software: you can redistribute it and/or modify\n");
        printf("it under the terms of the GNU General Public License as published by\n");
        printf("the Free Software Foundation, either version 3 of the License, or\n");
        printf("(at your option) any later version.\n\n");
        printf("This program is distributed in the hope that it will be useful,\n");
        printf("but WITHOUT ANY WARRANTY; without even the implied warranty of\n");
        printf("MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the\n");
        printf("GNU General Public License for more details.\n\n");
        printf("You should have received a copy of the GNU General Public License\n");
        printf("along with this program.  If not, see <https://www.gnu.org/licenses/>.\n\n");
    }
    /*
    else if(!strcmp(dbg, "d")){ //remove after debugging
        printf("pos->tokentype = %d, pos->data = %s\n", position->tokentype, position->data);
    } 
    */
    else {
        printf("Unrecognized command. Type 'help' for more information.\n");
        return dbg_prompt(ac, vartmp, parselist, position);
    }
    return dbg_prompt(ac, vartmp, parselist, position);
}

void show_code(tkelem *parselist, tkelem *position){
    tkelem *pt = parselist;
    tkelem *pos = position;
    
    while(pt != NULL){
        if(pt->tokentype == linebreak){
            printf("\n");
            if((pt->next == pos)||(pt->next->next == pos)){
                printf("    >");
            }
            printf("\t");
            if(pt->next->tokentype != label) printf("   ");
        } else if (pt->tokentype == vardef) {
            printf("{%s} ", pt->data);
        } else {
            printf("%s ", pt->data);
        }
        pt = pt->next;
    }
    printf("\n");
}

void exiting(tkelem *tklist, FILE *fpsrc, varelem *varlist, lbelem *lblist, int ecode){ //ecode == exit code
    printf("Exiting.\n");
    if(tklist != NULL) deleteall_tkelem(tklist);
    if(varlist != NULL) deleteall_varelem(varlist);
    if(lblist != NULL) deleteall_lbelem(lblist);
    if(fpsrc != NULL) fclose(fpsrc);
    exit(ecode);
}

//functions for working with linked lists
tkelem* insert_tkelem(tkelem *prevtk, token *tk){
    //static last elem?
    
    tkelem *tetmp;
    tetmp = (tkelem*)malloc(sizeof(tkelem));
    if(tetmp == NULL){
        printf("Malloc failed (insert_tkelem).\n");
        return NULL;
    }
    tetmp->prev = prevtk;
    if(prevtk == NULL) prevtk = tetmp; //if empty list
    else prevtk->next = tetmp;
    tetmp->tokentype = tk->tokentype;
    strcpy(tetmp->data, tk->data); //no secure function, but char[9] is written to char[9] and in every case nullterminated
    tetmp->next = NULL;
    return tetmp;
}

void deleteall_tkelem(tkelem *anchor){
    if(anchor == NULL) return;
    tkelem *pt, *hlp;
    for(pt = anchor; pt != NULL; pt = hlp){
        hlp = pt->next;
        free(pt);
    }
    if(DEBUG) printf("deleteall_tkelem(): list properly deleted.\n");
}

//insert in first position
varelem* insert_varelem(varelem *anchor, char *name, int val){
    varelem *vtmp;
    vtmp = (varelem*)malloc(sizeof(varelem));
    if(vtmp == NULL){
        printf("Malloc failed (insert_varelem).\n");
        return NULL;
    }
    vtmp->next = anchor;
    strcpy(vtmp->varname, name);
    vtmp->varvalue = val;
    anchor = vtmp;
    return anchor;
}

void deleteall_varelem(varelem *anchor){
    if(anchor == NULL) return;
    varelem *pt, *hlp;
    for(pt = anchor; pt != NULL; pt = hlp){
        hlp = pt->next;
        free(pt);
    }
    if(DEBUG) printf("deleteall_varelem(): list properly deleted.\n");
}

//insert in first position
lbelem* insert_lbelem(lbelem *anchor, char *name, tkelem *lnk){
    lbelem *lbtmp;
    lbtmp = (lbelem*)malloc(sizeof(lbelem));
    if(lbtmp == NULL){
        printf("Malloc failed (insert_lbelem).\n");
        return NULL;
    }
    lbtmp->next = anchor;
    strcpy(lbtmp->lbname, name);
    lbtmp->link = lnk;
    anchor = lbtmp;
    return anchor;
}

void deleteall_lbelem(lbelem *anchor){
    if(anchor == NULL) return;
    lbelem *pt, *hlp;
    for(pt = anchor; pt != NULL; pt = hlp){
        hlp = pt->next;
        free(pt);
    }
    if(DEBUG) printf("deleteall_lbelem(): list properly deleted.\n");
}
//end functions for linked lists
